---
title: 教你更换电脑的鼠标指针
tags:
  - 电脑技术
  - 电脑技巧
  - 鼠标
  - 个性化
categories:
  - 计算机
abbrlink: 3
date: 2021-12-25 11:10:00
img: /medias/article/shubiao.png
---
# 教你打造个性化鼠标
## 介绍
相信大家在使用电脑的时候会发现鼠标很单调，系统自带的只有黑白两个颜色的
现在，我教你使用看起来炫酷，牛逼的鼠标指针
## 注意
> 教程只适用于Windows系统，不适用于Linux、Macos
## 安装教程
>> 图片教程请访问http://zhutix.com/study/cursors-yan/
>> [点我跳转到图文教程](http://zhutix.com/study/cursors-yan/)
>>> <a href = 'http://zhutix.com/study/cursors-yan/'</a>
> 1.  打开链接 https://zhutix.com/tag/cursors/
>> [点我转到鼠标下载页面](https://zhutix.com/tag/cursors/)
> 2.  这里面有很多样式，你随便选一个，点进去，点下载
> 3.  下载好之后，打开所在文件夹
> 4.  使用压缩软件解压
> 5.  进入解压出来的文件夹里，右击`autorun.inf` ，点击安装，等待弹出窗口
> 6.  最后选择你刚装的鼠标就行
> ### 如果你看不懂文字教程，请观看视频教程
>> [点我跳转](https://zhutix.com/video/cursors-feng-yan)
## 说明
按照教程安装就可以了
如果你看不懂也没有关系
安装教程第四步解压出来后里面有详细教程的
教程里有图片
## 注意事项
> 1. 你可能会发现有些鼠标解压出来，进入了以后没有`autorun.inf` ，只有几个文件夹，那你就随便进入一个文件夹，里面就会有了
>> 如果你还是没找到`autorun.inf` ，也没有关系，因为他可能把名字改成了`右键安装.inf` ，右键安装就可以了
